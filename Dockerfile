FROM docker.elastic.co/elasticsearch/elasticsearch:7.16.1

ADD hunspell /usr/share/elasticsearch/config/hunspell

RUN bin/elasticsearch-plugin install --batch repository-s3
RUN bin/elasticsearch-plugin install --batch analysis-icu
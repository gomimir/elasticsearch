# Mimir Elasticsearch

This is a custom docker image of Elasticsearch.

We have added:

- repository-s3 plugin for backups to S3 storage
- analysis-icu plugin for improved unicode handling
- Czech hunspell dictionary for advanced text analysis

For more information about how to use it see [documentation](https://gomimir.gitlab.io).